#!/bin/bash

FAIL2BAN_CLIENT=/usr/bin/fail2ban-client
IPTABLES_DISCARDED_ZCAT="/srv/dev-disk-by-label-Disco1/InternalBackup/logarchive/iptables-discarded.log*.gz"
IPTABLES_DISCARDED_CAT="/var/log/iptables-discarded.log"

SORT=
QUIET=
UNIQ=

TOR_SERVER_LIST_WEBPAGE="https://check.torproject.org/torbulkexitlist"
TOR_SERVER_LIST=$(wget -O- -q "$TOR_SERVER_LIST_WEBPAGE")

PATH="$PATH:/usr/bin:/bin"

function checkIPs() {
	while read IP port; do
		echo -ne "${IP}\t"
		[ "$port" ] && echo -ne "\t${port}\t\t"
		if echo "$TOR_SERVER_LIST" | grep -q "$IP"; then
                       echo -e "\e[31myes\e[39m"
                else
                       echo -e "\e[32mno\e[39m"
                fi
	done
}

while getopts "squh" options; do
	case "${options}" in
		s)
		SORT="yes"
		;;

		q)
		QUIET="yes"
		;;

		u)
		SORT="yes"
		UNIQ="-u"
		;;

		h)
		echo -e "Usage: $0 [-s] [-q] [-u]\nPossible parameters:\n\t-s\tsort the IP addresses\n\t-q\tquiet Do not print the first line\n\t-u\tsort with uniq param (-s is assumed)\n\t-h\tthis help message"
		exit 0
	esac
done

[ "$QUIET" ] || echo -e "\e[93mChecking if banned IPs are Tor exit servers\e[39m"

#check fail2ban
sudo "$FAIL2BAN_CLIENT" status | grep "Jail list:" | cut -f2 | sed "s/, /\n/g" | while read jail; do sudo "$FAIL2BAN_CLIENT" status "$jail"; done | grep "Banned IP list:" | cut -f2 | grep "." | tr " " "\n" \
| ( [ "$SORT" ] && sort $UNIQ -n -t . -k 1,1 -k 2,2 -k 3,3 -k 4,4 || cat ) | checkIPs

[ "$QUIET" ] || echo

[ "$QUIET" ] || echo -e "\e[93mChecking if discarded IPs are Tor exit servers\e[39m"

#check logged discarded packet (country not in whitelist)
{ zcat $IPTABLES_DISCARDED_ZCAT; cat $IPTABLES_DISCARDED_CAT; } | while read row; do
	ip=$(  echo "$row" | awk -F 'SRC=' '{ print $2 }' | cut -d" " -f1)
	port=$(echo "$row" | awk -F 'DPT=' '{ print $2 }' | cut -d" " -f1)
	echo "$ip.$port"
done | ( [ "$SORT" ] && sort $UNIQ -n -t . -k 1,1 -k 2,2 -k 3,3 -k 4,4 -k 5,5 || cat ) | sed -r 's/(.*)\./\1 /'  | checkIPs

[ "$QUIET" ] || echo -ne "\n\n\e[93mNumber of IPs discarded: \e[39m"
[ "$QUIET" ] || { zcat $IPTABLES_DISCARDED_ZCAT; cat $IPTABLES_DISCARDED_CAT; } | wc -l
