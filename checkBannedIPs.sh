#!/bin/bash

FAIL2BAN_CLIENT=/usr/bin/fail2ban-client
SORT=
QUIET=

TOR_SERVER_LIST_WEBPAGE="https://check.torproject.org/torbulkexitlist"
TOR_SERVER_LIST=$(wget -O- -q "$TOR_SERVER_LIST_WEBPAGE")

PATH="$PATH:/usr/bin:/bin"

while getopts "sqh" options; do
        case "${options}" in
                s)
                SORT="yes"
                ;;

                q)
                QUIET="yes"
                ;;

                h)
                echo -e "Usage: $0 [-s] [-q]\nPossible parameters:\n\t-s\tsort the IP addresses\n\t-q\tquiet Do not print the first line\n\t-h\tthis help message"
                exit 0
        esac
done

[ "$QUIET" ] || echo "Checking if banned IPs are Tor exit servers"

sudo "$FAIL2BAN_CLIENT" status | grep "Jail list:" | cut -f2 | sed "s/, /\n/g" | while read jail
do
        sudo "$FAIL2BAN_CLIENT" status "$jail"
done | grep "Banned IP list:" | cut -f2 | grep "." | tr " " "\n" | while read IP; do
                echo -ne "${IP}\t"
                if test $(echo "$TOR_SERVER_LIST" | grep "$IP"); then
                       echo "yes"
                else
                       echo "no"
                fi
done | ( [ "$SORT" ] && sort -n -t . -k 1,1 -k 2,2 -k 3,3 -k 4,4 || cat )
